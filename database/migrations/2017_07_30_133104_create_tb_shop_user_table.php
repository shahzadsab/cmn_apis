<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbShopUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_shop_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('card_id');
            $table->enum('payment_mode_id',['easypaisa','free','cash']);
            $table->integer('package_id');
            $table->string('shop_url');
            $table->boolean('shop_access');
            $table->boolean('shop_banner');
            $table->string('confirm_code');
            $table->string('banner');
            $table->string('facebook_url');
            $table->string('twitter_url');
            $table->string('linkedin_url');
            $table->string('googleplus_url');
            $table->date('expiry_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_shop_user');
    }
}
