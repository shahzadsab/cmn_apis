<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbCardUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_card_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('business_type_id');
            $table->integer('category_id');
            $table->integer('subcategory_id');
            $table->integer('country_id');
            $table->integer('state_id');
            $table->integer('city_id');
            $table->integer('zone_id');
            $table->string('contact_person');
            $table->string('desigantion');
            $table->string('company_name');
            $table->string('address');
            $table->string('mobile');
            $table->string('land_line');
            $table->string('email');
            $table->string('site');
            $table->string('logo');
            $table->string('logo_alt');
            $table->string('email_verification_code');
            $table->string('mobile_verification_code');
            $table->boolean('email_status');
            $table->boolean('mobile_status');
            $table->boolean('status');
            $table->string('deals');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_card_user');
    }
}
